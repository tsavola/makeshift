#
# Makeshift 1.4 <http://iki.fi/timo.savola/projects/makeshift>
#
# The following license does not restrict the distribution of software built
# with the help of this script.
#
# Copyright (c) 2006-2007 Timo Savola <timo.savola@iki.fi>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#

CROSS_COMPILE	?= 
CCACHE		?= $(if $(shell which ccache),ccache,)
CC		?= $(CROSS_COMPILE)gcc
CXX		?= $(CROSS_COMPILE)g++
AR		?= $(CROSS_COMPILE)ar

CTAGS		?= ctags-exuberant
CTAGS_FLAGS	?= --extra=+q --c++-kinds=+px
ETAGS		?= $(CTAGS) -e
ETAGS_FLAGS	?= $(CTAGS_FLAGS)
ETAGS_INCLUDEOPT?= --etags-include

TARGET		?= $(shell $(CC) -dumpmachine)-$(shell $(CC) -dumpversion)
TARGETBASE	?= target
TARGETDIR	:= $(TARGETBASE)/$(TARGET)

# Profile

PROFILES	:= 

define do_profile

include		$(1)

ifeq ($$(findstring $$(notdir $(1:.mk=)),$$(PROFILES)),)
PROFILES	+=  $$(notdir $(1:.mk=))
endif

endef

profiles = $(foreach FILE,$(wildcard $(1)),$(eval $(call do_profile,$(FILE))))

# Standard profiles

MAKESHIFT	?= $(BUILD)

$(call profiles,$(MAKESHIFT)/profile/*.mk)

# Default

build:

.PHONY: build

# Verbosity

VERBOSE		?= 

ifeq ($(VERBOSE),0)
VERBOSE := 
endif

ifeq ($(VERBOSE),)
define echo
	@ printf "  %-8s  %s\n" "$(1)" "$(subst $(TARGETDIR)/,,$(2))"
endef
else
define echo
endef
endif

ECHO_1 := 
ECHO_2 := 

ifeq ($(VERBOSE),)
ECHO_1 := @
ECHO_2 := @
endif

ifeq ($(VERBOSE),1)
ECHO_2 := @
endif

# General

EMPTY_VARIABLE		:= 
SPACE_CHARACTER		:= $(EMPTY_VARIABLE) $(EMPTY_VARIABLE)

CXXPATTERNS		:= %.cc %.cp %.cxx %.cpp %.CPP %.c++ %.C
ALLPATTERNS		:= %.c $(CXXPATTERNS)

do_object_suffix	= $(filter-out $(ALLPATTERNS),$(foreach PAT,$(ALLPATTERNS),$(patsubst $(PAT),$(2),$(1))))

do_depend_symbol	= $(word 1,$(subst :,$(SPACE_CHARACTER),$(word 1,$(subst @,$(SPACE_CHARACTER),$(3)))))
do_depend_profile	= $(word 2,$(subst :,$(SPACE_CHARACTER),$(word 1,$(subst @,$(SPACE_CHARACTER),$(3)))) $(2))
do_depend_type		= $(if $(findstring static,$(word 2,$(subst @,$(SPACE_CHARACTER),$(3)))),STATIC_LIBRARY,SHARED_GENERIC)
do_depend_libdir	= $($(call do_depend_symbol,$(1),$(2),$(3))_TARGETDIR)/$(call do_depend_profile,$(1),$(2),$(3))/lib
do_depend_lib		= $(call do_depend_libdir,$(1),$(2),$(3))/$($(call do_depend_symbol,$(1),$(2),$(3))_$(call do_depend_type,$(1),$(2),$(3))_NAME)
do_depend_libs		= $(foreach DEP,$($(1)_DEPENDS_$(2)),$(call do_depend_lib,$(1),$(2),$(DEP)))

do_libpath		= $(subst $(SPACE_CHARACTER),:,$($(1)_LIBDIRS_$(2)))

define do_general

$(1)_DEPENDS_$(2)	+= $$(DEPENDS_$(2)) $$($(1)_DEPENDS)
$(1)_LIBDIRS_$(2)	+= $$(foreach DEP,$$($(1)_DEPENDS_$(2)),$$(call do_depend_libdir,$(1),$(2),$$(DEP)))
$(1)_LIBPATH_$(2)	+= $$(call do_libpath,$(1),$(2))

$(1)_CPPFLAGS_$(2)	+= $$(CPPFLAGS_$(2)) $$($(1)_CPPFLAGS) -I.
$(1)_CCFLAGS_$(2)	+= $$(CCFLAGS_$(2))  $$($(1)_CCFLAGS)  $$(CFLAGS_$(2)) $$($(1)_CFLAGS)
$(1)_CXXFLAGS_$(2)	+= $$(CXXFLAGS_$(2)) $$($(1)_CXXFLAGS) $$(CFLAGS_$(2)) $$($(1)_CFLAGS)
$(1)_LDFLAGS_$(2)	+= $$(LDFLAGS_$(2))  $$($(1)_LDFLAGS)  $$($(1)_LIBDIRS_$(2):%=-L%)
$(1)_LIBS_$(2)		+= $$(LIBS_$(2))     $$($(1)_LIBS)

$(1)_STATIC_OBJECTS_$(2)	:= $$(foreach SOURCE,$$($(1)_SOURCES),$$($(1)_TARGETDIR)/$(2)/obj/$$($(1)_NAME)/$$(call do_object_suffix,$$(SOURCE),%.o))
$(1)_SHARED_OBJECTS_$(2)	:= $$(foreach SOURCE,$$($(1)_SOURCES),$$($(1)_TARGETDIR)/$(2)/obj/$$($(1)_NAME)/$$(call do_object_suffix,$$(SOURCE),%.os))

-include $$($(1)_STATIC_OBJECTS_$(2):.o=.d)

$(1)_CC			?= $$(CACHE) $$(CC)
$(1)_CXX		?= $$(CACHE) $$(CXX)

ifeq ($$(filter $(CXXPATTERNS),$$($(1)_SOURCES)),)
$(1)_LD			?= $$($(1)_CC)
$(1)_LD_CFLAGS_$(2)	:= $$($(1)_CCFLAGS_$(2))
else
$(1)_LD			?= $$($(1)_CXX)
$(1)_LD_CFLAGS_$(2)	:= $$($(1)_CXXFLAGS_$(2))
endif

echo-$(1)-$(2)-libpath:
	@ echo $$($(1)_LIBPATH_$(2))

.PHONY: echo-$(1)-$(2)-libpath

endef

# Object

define do_object
ifeq ($$(filter $$(CXXPATTERNS),$(3)),)

$$(TARGETDIR)/$(2)/obj/$$($(1)_NAME)/$(3:.c=.o): $(3)
	$$(call echo,Compile,$$@)
	$$(ECHO_2) mkdir -p $$(dir $$@)
	$$(ECHO_1) $$($(1)_CC) $$($(1)_CPPFLAGS_$(2)) $$($(1)_CCFLAGS_$(2)) -c -MMD -MF $$(patsubst %.o,%.d,$$@) -MT $$@ -o $$@ $(3)

$$(TARGETDIR)/$(2)/obj/$$($(1)_NAME)/$(3:.c=.os): $(3)
	$$(call echo,Compile,$$@)
	$$(ECHO_2) mkdir -p $$(dir $$@)
	$$(ECHO_1) $$($(1)_CC) $$($(1)_CPPFLAGS_$(2)) -DPIC $$($(1)_CCFLAGS_$(2)) -fPIC -c -MMD -MF $$(patsubst %.os,%.d,$$@) -MT $$@ -o $$@ $(3)

else

$$(TARGETDIR)/$(2)/obj/$$($(1)_NAME)/$$(call do_object_suffix,$(3),%.o): $(3)
	$$(call echo,Compile,$$@)
	$$(ECHO_2) mkdir -p $$(dir $$@)
	$$(ECHO_1) $$($(1)_CXX) $$($(1)_CPPFLAGS_$(2)) $$($(1)_CXXFLAGS_$(2)) -c -MMD -MF $$(patsubst %.o,%.d,$$@) -MT $$@ -o $$@ $(3)

$$(TARGETDIR)/$(2)/obj/$$($(1)_NAME)/$$(call do_object_suffix,$(3),%.os): $(3)
	$$(call echo,Compile,$$@)
	$$(ECHO_2) mkdir -p $$(dir $$@)
	$$(ECHO_1) $$($(1)_CXX) $$($(1)_CPPFLAGS_$(2)) -DPIC $$($(1)_CXXFLAGS_$(2)) -fPIC -c -MMD -MF $$(patsubst %.os,%.d,$$@) -MT $$@ -o $$@ $(3)

endif
endef

do_objects = $(foreach SOURCE,$(3),$(eval $(call do_object,$(1),$(2),$(SOURCE))))

# Library

define do_export_library

$(1)_TARGETDIR			?= $$(TARGETDIR)

$(1)_STATIC_LIBRARY_NAME	:= lib$$($(1)_NAME).a
$(1)_STATIC_LIBRARY_$(2)	:= $$($(1)_TARGETDIR)/$(2)/lib/$$($(1)_STATIC_LIBRARY_NAME)

$(1)_SHARED_GENERIC_NAME	:= lib$$($(1)_NAME).so
$(1)_SHARED_GENERIC_$(2)	:= $$($(1)_TARGETDIR)/$(2)/lib/$$($(1)_SHARED_GENERIC_NAME)

endef

export_library = $(foreach PROFILE,$(PROFILES),$(eval $(call do_export_library,$(1),$(PROFILE))))

define do_library

$$(eval $$(call do_export_library,$(1),$(2)))
$$(eval $$(call do_general,$(1),$(2)))

$(1)_SHARED_LIBRARY_NAME	:= $$($(1)_SHARED_GENERIC_NAME).$$($(1)_VERSION)
$(1)_SHARED_LIBRARY_$(2)	:= $$($(1)_TARGETDIR)/$(2)/lib/$$($(1)_SHARED_LIBRARY_NAME)

$$(eval $$(call do_objects,$(1),$(2),$$($(1)_SOURCES)))

all: $(1)
all-$(2): $(1)-$(2)
all-$(2)-static: $(1)-$(2)-static
all-$(2)-shared: $(1)-$(2)-shared
$(1): $(1)-$(2)
$(1)-$(2): $(1)-$(2)-static $(1)-$(2)-shared
$(1)-$(2)-static: $$($(1)_STATIC_LIBRARY_$(2))
$(1)-$(2)-shared: $$($(1)_SHARED_GENERIC_$(2))

.PHONY: all all-$(2) all-$(2)-static all-$(2)-shared $(1) $(1)-$(2) $(1)-$(2)-static $(1)-$(2)-shared

$$($(1)_STATIC_LIBRARY_$(2)): $$($(1)_STATIC_OBJECTS_$(2))
	$$(call echo,Archive,$$@)
	$$(ECHO_2) mkdir -p $$(dir $$@)
	$$(ECHO_1) $$(AR) crs $$@ $$($(1)_STATIC_OBJECTS_$(2))

$$($(1)_SHARED_LIBRARY_$(2)): $$($(1)_SHARED_OBJECTS_$(2)) $$(call do_depend_libs,$(1),$(2))
	$$(call echo,Link,$$@)
	$$(ECHO_2) mkdir -p $$(dir $$@)
	$$(ECHO_1) $$($(1)_LD) $$($(1)_LD_CFLAGS_$(2)) -fPIC $$($(1)_LDFLAGS_$(2)) -shared -Wl,-soname,$$($(1)_SHARED_LIBRARY_NAME) -o $$@ $$($(1)_SHARED_OBJECTS_$(2)) $$($(1)_LIBS_$(2))
	$$(ECHO_2) chmod -x $$@

$$($(1)_SHARED_GENERIC_$(2)): $$($(1)_SHARED_LIBRARY_$(2))
	$$(ECHO_1) echo "INPUT($$($(1)_SHARED_LIBRARY_NAME))" > $$@

endef

library = $(foreach PROFILE,$(PROFILES),$(eval $(call do_library,$(1),$(PROFILE))))

# Binary

define do_build_binary

$(1)_TARGETDIR		?= $$(TARGETDIR)
$(1)_BINARY_$(2)	:= $$($(1)_TARGETDIR)/$(2)/bin/$$($(1)_NAME)

$$(eval $$(call do_general,$(1),$(2)))
$$(eval $$(call do_objects,$(1),$(2),$$($(1)_SOURCES)))

$$($(1)_BINARY_$(2)): $$($(1)_STATIC_OBJECTS_$(2)) $$(call do_depend_libs,$(1),$(2))
	$$(call echo,Link,$$@)
	$$(ECHO_2) mkdir -p $$(dir $$@)
	$$(ECHO_1) $$($(1)_LD) $$($(1)_LD_CFLAGS_$(2)) $$($(1)_LDFLAGS_$(2)) -o $$@ $$($(1)_STATIC_OBJECTS_$(2)) $$($(1)_LIBS_$(2))

echo-$(1)-$(2)-binary:
	@ echo $$($(1)_BINARY_$(2))

.PHONY: echo-$(1)-$(2)-binary

endef

define do_binary

$$(eval $$(call do_build_binary,$(1),$(2)))

all: $(1)
all-$(2): $(1)-$(2)
$(1): $(1)-$(2)
$(1)-$(2): $$($(1)_BINARY_$(2))

.PHONY: all all-$(2) $(1) $(1)-$(2)

endef

binary = $(foreach PROFILE,$(PROFILES),$(eval $(call do_binary,$(1),$(PROFILE))))

# Test

define do_test

$$(eval $$(call do_build_binary,$(1),$(2)))

$(1)_TEST_$(2)	:= $$(TARGETDIR)/$(2)/test/$$($(1)_NAME)

check: $(1)
check-$(2): $(1)-$(2)
$(1): $(1)-$(2)
$(1)-$(2): $$($(1)_TEST_$(2))

.PHONY: check check-$(2) $(1) $(1)-$(2)

$$($(1)_TEST_$(2)): $$($(1)_BINARY_$(2))
	$$(call echo,Check,$$<)
	$$(ECHO_2) mkdir -p $$(dir $$@)
	$$(ECHO_1) LD_LIBRARY_PATH=$$($(1)_LIBPATH_$(2)) $$<
	$$(ECHO_2) touch $$@

endef

test = $(foreach PROFILE,$(PROFILES),$(eval $(call do_test,$(1),$(PROFILE))))

# Tags

TAGS_SOURCES	:= 
TAGS_INCLUDES	:= 

etags: TAGS
ctags: tags

.PHONY: etags ctags

TAGS: $(TAGS_SOURCES)
	$(call echo,Generate,$@)
	$(ECHO_1) $(ETAGS) $(foreach DIR,$(TAGS_INCLUDES),$(ETAGS_INCLUDEOPT)=$(realpath $(DIR))/TAGS) $(ETAGS_FLAGS) -f $@ $(TAGS_SOURCES)

tags: $(TAGS_SOURCES)
	$(call echo,Generate,$@)
	$(ECHO_1) $(CTAGS) $(CTAGS_FLAGS) -f $@ $(TAGS_SOURCES)

# Install

PREFIX		?= /usr/local
DESTDIR		?= 

install:

.PHONY: install

# Clean

clean:
ifneq ($(wildcard $(TARGETBASE)),)
	$(call echo,Remove,$(TARGETBASE))
	$(ECHO_1) rm -r $(TARGETBASE)
endif
ifneq ($(wildcard TAGS),)
	$(call echo,Remove,TAGS)
	$(ECHO_1) rm TAGS
endif
ifneq ($(wildcard tags),)
	$(call echo,Remove,tags)
	$(ECHO_1) rm tags
endif

.PHONY: clean
