CPPFLAGS_optimize	= $(CPPFLAGS) -DNDEBUG
CFLAGS_optimize		= $(CFLAGS) -O2 -fomit-frame-pointer
CCFLAGS_optimize	= $(CCFLAGS)
CXXFLAGS_optimize	= $(CXXFLAGS) -finline-functions
LDFLAGS_optimize	= $(LDFLAGS) -s
LIBS_optimize		= $(LIBS)
DEPENDS_optimize	= 
