PREFIX	:= /usr/local
DESTDIR	:= 

install:
	install -d $(DESTDIR)$(PREFIX)/share/makeshift/profile
	install build.mk $(DESTDIR)$(PREFIX)/share/makeshift/
	install profile/*.mk $(DESTDIR)$(PREFIX)/share/makeshift/profile/

VERSION	= $(shell grep "This file is part of Makeshift" build.mk \
		| sed -r "s/.*Makeshift ([0-9\.]+)[ ]?.*/\1/")

docs:
	VERSION=$(VERSION) doxygen

install-docs: docs
	install -d $(DESTDIR)$(PREFIX)/share/doc
	rm -rf $(DESTDIR)$(PREFIX)/share/doc/makeshift
	cp -r target/html $(DESTDIR)$(PREFIX)/share/doc/makeshift

.PHONY: install docs install-docs
